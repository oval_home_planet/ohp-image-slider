(function($) {
    $.fn.ohp_slider = function(options){
        var defaults = {
        	width  : 80,
            wait   : 2000,
            speed  : 800,
            easing : 'linear',
            caption: true,
            opacity: 0.5
        };
        var setting = $.extend(defaults, options);
		if ( setting.width < 35 && setting.width != 'auto' ){
			setting.width = 35;
		}

//-------------------------------

		var self = $(this);

		var clickCancel = 0;
		var loop;

		var ul = self.children('ul');

		ul.css('height', 0);
		ul.after('<div class="slider-prev"></div><div class="slider-next"></div>');

		var imgCount = ul.children('li').length;
		ul.children('li:last').clone(true).insertBefore(ul.children('li:first'));
		if ( imgCount < 3 ){
			ul.children('li:nth-child(2)').clone(true).insertAfter(ul.children('li:last'));
			ul.children('li:nth-child(3)').clone(true).insertAfter(ul.children('li:last'));
		}
		imgCount = ul.children('li').length;

//-------------------------------

		var originalImgWidth  = ul.children('li').find('img').width();
		var originalImgHeight = ul.children('li').find('img').height();

//-------------------------------

		$(window).on('load resize', function(){
			if ( setting.width != 'auto' ){
				ul.children('li').css('width', self.width() * (setting.width*0.01));
			}else{
				var imgPer = ul.children('li').height() / originalImgHeight;

				ul.children('li').find('img').css('width', Math.round(originalImgWidth * imgPer));
				ul.children('li').css('width', ul.children('li').find('img').width());
				ul.children('li').find('img').css('height', '100%');
			}

			var imgWidth = ul.children('li').width();

			caption.css('paddingLeft', (self.width() - imgWidth + 20) / 2).css('paddingRight', (self.width() - imgWidth + 20) / 2);

			ul.css('width', imgWidth * imgCount);
			ul.css('height', ul.children('li').height());
			ul.css('marginLeft', -(imgWidth - ((self.width() - imgWidth) / 2)));
		});

//-------------------------------

		ul.children('li').eq(0).css('opacity', setting.opacity);
		var i = 2;
		while ( i < imgCount ){
			ul.children('li').eq(i).css('opacity', setting.opacity);
			i++;
		}

		if ( setting.caption == true ){
			ul.after('<div class="caption"><div></div></div>');
			var caption = self.children('.caption').children('div');
			self.children('.caption').css('bottom', -($('.caption').height()));

			var imgAlt = ul.children('li').eq(1).find('img').attr('alt');

			if ( imgAlt.length >= 1 ){
				caption.html(imgAlt);
				self.children('.caption').css('bottom', 0);
			}
		}

//-------------------------------

		$(window).on('load', function(){
			startLoop();
		});

//-------------------------------

		function startLoop(){
			stopLoop();

			loop = setInterval(function() {
				if ( clickCancel == 0 ){
					slideChange('next');
				}
			}, setting.wait+setting.speed);
		}

		function stopLoop(){
			clearInterval(loop);
		}

//-------------------------------

		function slideChange(prevNext){
			clickCancel = 1;
			stopLoop();

			var imgWidth = ul.children('li').width();

			if ( prevNext == 'prev' ){
				var array = [1,2,1,0];
			}else{
				var array = [2,1,2,-(imgWidth)];
			}

			if ( prevNext == 'prev' ){
				ul.children('li:last').remove();
				ul.children('li:last').clone(true).insertBefore(ul.children('li:first'));
				ul.children('li:first').css('marginLeft', -(imgWidth)).css('opacity', setting.opacity);
			}

			var imgAlt   = ul.children('li').eq(array[0]).find('img').attr('alt');

			if ( imgAlt.length >= 1 ){
				caption.html(imgAlt);
				self.children('.caption').animate({
					bottom : 0
				}, setting.speed);
			}else{
				self.children('.caption').animate({
					bottom : -($('.caption').height())
				}, setting.speed);
			}

			ul.children('li').eq(array[1]).animate({
				opacity : setting.opacity
			}, setting.speed);

			ul.children('li').eq(array[2]).animate({
				opacity : 1
			}, setting.speed);

			ul.children('li:first').animate({
				marginLeft : array[3]
			}, {
			    duration : setting.speed,
			    easing   : setting.easing,
			    complete : function() {
			    	if ( prevNext == 'next' ){
			    		ul.children('li:first').remove();
				        ul.children('li:first').clone().insertAfter(ul.children('li:last'));
			    	}
				    clickCancel = 0;
				    startLoop();
			    }
			});
		}

//-------------------------------

		$(this).find('.slider-prev').click(function(){
			if ( clickCancel == 0 ){
				slideChange('prev');
			}
		});

		$(this).find('.slider-next').click(function(){
			if ( clickCancel == 0 ){
				slideChange('next');
			}
		});

		$(this).mouseover(function(){
			stopLoop();
		});

		$(this).mouseout(function(){
			startLoop();
		});

//-------------------------------

        return(this);
    };
})(jQuery);