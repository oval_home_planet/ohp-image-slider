// OHP Slider ver.1.2
(function($) {
    $.fn.ohp_slider = function(options){
        var defaults = {
        	width  : 80,
            wait   : 2000,
            speed  : 800,
            easing : 'linear',
            caption: true,
            opacity: 0.5,
            buttons: false
        };
        var setting = $.extend(defaults, options);
		if ( setting.width < 35 && setting.width != 'auto' ){
			setting.width = 35;
		}

//-------------------------------

		// 初期設定
		var self = $(this).attr('role','toolbar');

		var clickCancel = 0;
		var loop;
		var originalImgWidth;
		var originalImgHeight;
		var slideID = 1;

		self.addClass('ohp-slider');

		var ul = self.children('ul');

		ul.css('height', 0);
		ul.after('<div class="slider-prev" aria-label="Previous"></div><div class="slider-next" aria-label="Next"></div>');

		// スライド数を取得
		var imgCount = ul.children('li').length;
		var imgCountOriginal = imgCount;

		// 最初のliの前に最後のliのクローンを作成
		ul.children('li:last').clone(true).insertBefore(ul.children('li:first'));

		// スライド数が3未満の場合、2つ分のスライドを追加
		if ( imgCount < 3 ){
			ul.children('li:nth-child(2)').clone(true).insertAfter(ul.children('li:last'));
			ul.children('li:nth-child(3)').clone(true).insertAfter(ul.children('li:last'));
		}

		// 最終的なスライド数を取得
		imgCount = ul.children('li').length;

		// スライドにidを割り振る
		ul.children('li').each(function( i ){
			$(this).attr('data-id',i);
		});

		// ボタン表示を行う場合
		if ( setting.buttons == true ){
			var buttons = '<li role="presentation" aria-hidden="false" aria-selected="true" data-id="1"></li>';

			for ( var i=2; i<=imgCountOriginal; i++ ){
				buttons += '<li role="presentation" aria-hidden="true" aria-selected="false" data-id="'+i+'"></li>';
			}

			ul.after('<ul class="buttons" role="tablist">'+buttons+'</ul>');
		}

//-------------------------------

		$(window).on('load', function(){
			originalImgWidth  = ul.children('li').find('img').width();
			originalImgHeight = ul.children('li').find('img').height();
		});

//-------------------------------

		$(window).on('load resize', function(){
			if ( setting.width != 'auto' ){
				ul.children('li').css('width', self.width() * (setting.width*0.01));
			}else{
				var imgPer = ul.children('li').height() / originalImgHeight;

				ul.children('li').find('img').css('width', Math.round(originalImgWidth * imgPer));
				ul.children('li').css('width', ul.children('li').find('img').width());
				ul.children('li').find('img').css('height', '100%');
			}

			var imgWidth = ul.children('li').width();

			caption.css('paddingLeft', (self.width() - imgWidth + 20) / 2).css('paddingRight', (self.width() - imgWidth + 20) / 2);

			ul.css('width', imgWidth * imgCount);
			ul.css('height', ul.children('li').height());
			ul.css('marginLeft', -(imgWidth - ((self.width() - imgWidth) / 2)));
		});

//-------------------------------

		ul.children('li').eq(0).css('opacity', setting.opacity).attr('role','option').attr('aria-hidden','false');
		var i = 2;
		while ( i < imgCount ){
			ul.children('li').eq(i).css('opacity', setting.opacity).attr('role','option').attr('aria-hidden','true');
			i++;
		}

		if ( setting.caption == true ){
			ul.after('<div class="caption"><div></div></div>');
			var caption = self.children('.caption').children('div');
			self.children('.caption').css('bottom', -($('.caption').height()));

			var imgAlt = ul.children('li').eq(1).find('img').attr('alt');

			if ( imgAlt.length >= 1 ){
				caption.html(imgAlt);
				self.children('.caption').css('bottom', 0);
			}
		}

//-------------------------------

		$(window).on('load', function(){
			startLoop();
		});

//-------------------------------

		// 自動ループ開始
		function startLoop(){
			stopLoop();

			loop = setInterval(function() {
				if ( clickCancel == 0 ){
					slideChange('next', 1, 1);
				}
			}, setting.wait+setting.speed);
		}

		// 自動ループ停止
		function stopLoop(){
			clearInterval(loop);
		}

//-------------------------------

		function slideChange(prevNext, maxCount, i){
			clickCancel = 1;
			stopLoop();

			var imgWidth = ul.children('li').width();

			if ( prevNext == 'prev' ){
				var array = [1,2,1,0];
			}else{
				var array = [2,1,2,-(imgWidth)];
			}

			if ( i < maxCount ){
				var easing = 'linear';
				var speed  = setting.speed / 10;
			}else{
				var easing = setting.easing;
				var speed  = setting.speed;
			}

			if ( prevNext == 'prev' ){
				ul.children('li:last').remove();
				ul.children('li:last').clone(true).insertBefore(ul.children('li:first'));
				ul.children('li:first').css('marginLeft', -(imgWidth)).css('opacity', setting.opacity);
			}

			var imgAlt   = ul.children('li').eq(array[0]).find('img').attr('alt');

			if ( imgAlt.length >= 1 ){
				caption.html(imgAlt);
				self.children('.caption').animate({
					bottom : 0
				}, speed);
			}else{
				self.children('.caption').animate({
					bottom : -($('.caption').height())
				}, speed);
			}

			ul.children('li').eq(array[1]).animate({
				opacity : setting.opacity
			}, speed).attr('aria-hidden','true');

			ul.children('li').eq(array[2]).animate({
				opacity : 1
			}, speed).attr('aria-hidden','false');

			ul.children('li:first').animate({
				marginLeft : array[3]
			}, {
			    duration : speed,
			    easing   : easing,
			    complete : function() {
			    	if ( prevNext == 'next' ){
			    		ul.children('li:first').remove();
				        ul.children('li:first').clone().insertAfter(ul.children('li:last'));
			    	}

					slideID = ul.children('li').eq(1).attr('data-id');

					self.children('ul.buttons').children('li').each(function( i ){
						if ( $(this).attr('data-id') == slideID ){
							$(this).attr('aria-hidden',false).attr('aria-selected',true);
						}else{
							$(this).attr('aria-hidden',true).attr('aria-selected',false);
						}
					});

					// 繰り返しするか？
					if ( i < maxCount ) {
						slideChange(prevNext, maxCount, ++i);
					}else{
					    clickCancel = 0;
					    startLoop();
					}
			    }
			});
		}

//-------------------------------

		$(this).find('.slider-prev').click(function(){
			if ( clickCancel == 0 ){
				slideChange('prev', 1, 1);
			}
		});

		$(this).find('.slider-next').click(function(){
			if ( clickCancel == 0 ){
				slideChange('next', 1, 1);
			}
		});

		$(this).find('.buttons li').click(function(){
			if ( clickCancel == 0 ){
				clickCancel = 1;
				stopLoop();

				if ( $(this).attr('data-id') > slideID ){
					var count = $(this).attr('data-id') - slideID;
					slideChange('next', count, 1);
				}else if ( $(this).attr('data-id') < slideID ){
					var count = slideID - $(this).attr('data-id');
					slideChange('prev', count, 1);
				}else{
					clickCancel = 0;
					startLoop();
				}
			}
		});

//-------------------------------

        $(this).bind("touchstart", onTouchStart);
    	$(this).bind("touchmove", onTouchMove);
    	$(this).bind("touchend", onTouchEnd);

    	var position;
     
    	//スワイプ開始時の横方向の座標を格納
    	function onTouchStart(event) {
    	    position = getPosition(event);
    	}

    	function getPosition(event) {
			return event.originalEvent.touches[0].pageX;
		}
     
    	//スワイプの方向（left／right）を取得
    	function onTouchMove(event) {
    	    if ( clickCancel == 0 ){
    	        if ( position > getPosition(event) ){
    	            slideChange('next', 1, 1);
    	        }else{
    	            slideChange('prev', 1, 1);
    	        }
    	    }
    	}

//-------------------------------

		$(this).mouseover(function(){
			stopLoop();
		});

		$(this).mouseout(function(){
			startLoop();
		});

//-------------------------------

        return(this);
    };
})(jQuery);